from flask import Flask, render_template, jsonify
from model import clock

app = Flask(__name__)


# Routes
@app.route('/')
def home():
    return render_template('home.html')


@app.route('/api/json/quote')
def api_get_quote():
    quote = clock.get_quote()
    return jsonify(text=quote.text,
                   author=quote.author,
                   book=quote.source)


if __name__ == '__main__':
    app.run()
