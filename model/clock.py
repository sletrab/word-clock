# Clock logic
from time import localtime, strftime
from model.quote import Quote
from model.quote_source import QuoteSource

# Load quote source from file
source = QuoteSource('litclock_annotated.csv')


# Find the appropriate quote
def find_current_quote_item():
    time_as_string = get_current_time_as_string()
    for quote in source.get_quotes_list():
        # TODO: It's possible that there are multiple entries for the current minute. Choose randomly
        if quote.time == time_as_string:
            return quote

    # If no fitting time is found
    return Quote()


# Get the current time as a string
def get_current_time_as_string():
    return strftime("%H:%M", localtime())


def get_quote():
    return find_current_quote_item()

