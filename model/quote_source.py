import csv
from .quote import Quote


class QuoteSource:

    def __init__(self, filename):
        self.filename = filename
        self.quotes_list = []

        # Read data from CSV file
        with open(self.filename, "r") as file:
            reader = csv.reader(file, delimiter='|', quotechar='"')
            for line in reader:
                my_quote = Quote(line[0],  # Time
                                 line[1],  # Time in Sentence
                                 line[2],  # Quote
                                 line[3],  # Author
                                 line[4])  # Source

                # Insert strong tag around time
                pos_start = my_quote.text.lower().find(my_quote.time_in_sentence.lower())
                pos_end = pos_start + len(my_quote.time_in_sentence)
                new_text = my_quote.text[:pos_end] + '</strong>' + my_quote.text[pos_end:]
                new_text = new_text[:pos_start] + '<strong>' + new_text[pos_start:]
                my_quote.text = new_text

                self.quotes_list.append(my_quote)

    def get_quotes_list(self):
        return self.quotes_list

    def __len__(self):
        return len(self.quotes_list)
