# Quote Class
class Quote:

    def __init__(self,
                 time: str = "00:00",
                 time_in_sentence: str = "midnight",
                 text: str = "No Quote",
                 author: str = "No author",
                 source: str = "No source"):
        self.time = time
        self.time_in_sentence = time_in_sentence
        self.text = text
        self.author = author
        self.source = source

    def __str__(self):
        return "Quote(Text: " + self.text + ", Author: " + self.author + ", Source: " + self.source + ")\n"

    def __repr__(self):
        return self.__str__()

    def __len__(self):
        pass