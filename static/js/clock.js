;
(function () {

    // Handle font-size by orientation
    if (window.innerWidth > window.innerHeight) {
        $('#text').addClass('landscape');
    }

    // Refresh displayed quote
    function refreshQuote() {
      $.get( "/api/json/quote", function( quote ) {
          $("#text").html( quote.text );
          $("#book").html( quote.book );
          $("#author").html( quote.author );
          setMargin();
        });
    }
    var interval = 1000;
    setInterval(refreshQuote, interval);

    // Set margin-top of quote to center it
    function setMargin() {
      var quoteHeight = $('#quote').height();
      var offset = (window.innerHeight - quoteHeight) / 2;
      $('#quote').css('margin-top', 'calc(50% - '+offset+'px)')
    }
})();